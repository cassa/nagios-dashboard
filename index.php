<?php
//The dashboard
require_once("./functions.php");
?>

<!DOCTYPE html>
<html>
<head>
    <title>Nagios dashboard</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="refresh" content="30">
    <link href="//netdna.bootstrapcdn.com/bootswatch/3.0.2/united/bootstrap.min.css" rel="stylesheet">
    <style type="text/css">
        a {
            color: #333 !important;
        }
        .alert { 
            padding:5px 13px !important;
            border-radius: 0px !important;
            margin-bottom: 2px !important;
        }
    </style>
</head>
<body>

    <div class="row">
        <div class="col-md-12">
            <?php
            if($criticals_count > 0 || $host_issue_count > 0) {
                print('<div class="alert alert-danger"><h2>');
                if($criticals_count > 0) {
                    print(($criticals_count));
                    print(" Critical Issues. ");
                }
                if ($host_issue_count > 0) {
                 print($host_issue_count . " Hosts Down!");
             } 
             print("<h2></div>");
         } elseif(($warnings_count) > 0) {
            print(($warnings_count));
            print(" Non Critical Issues</h2></div>");
        } else {
            print('<div class="alert alert-success"><h2>');
            print(" Everything Running Fine!</h2></div>");
        }
        ?>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <?php
        host_alert_cards("Hosts Down", "danger", $host_issue_count, $host_issues);
        service_alert_cards("Critical Issues", "danger", $criticals_count, $criticals);
        host_alert_cards("Hosts Acknowledged Down", "info", $host_ack_issues_count, $host_ack_issues);
        ?>
    </div>
    <div class="col-md-6">
        <?php
        service_alert_cards("Minor Issues ", "warning", $warnings_count, $warnings);
        service_alert_cards("Acknowledged Minor Issues", "info", $warnings_ack_count, $warnings_ack_issues);
        service_alert_cards("Acknowledged Criticals", "info", $criticals_ack_count, $criticals_ack);
        ?>
    </div>
    <div class="col-md-6">
        <?php
        ?>
    </div>
</div>


</body>
</html>
